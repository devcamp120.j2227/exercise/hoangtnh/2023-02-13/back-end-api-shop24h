const jwt = require("jsonwebtoken")
require("dotenv").config();
const jwtModel = require("../model/jwtModel")
const authenAdminToken = (request, response, next) => {
    const authorizationHeader = request.headers["authorization"];
    
    const token = authorizationHeader.split(" ")[1];
    if(!token){
        return response.status(401).json({
            status:"Error",
            message: "Token is not valid"
        })
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, data)=>{
        if(error){
            return response.status(401).json({
                status:"Error",
                message: "Token is not valid"
            })
        }
        if(data){
            console.log("verfy",data)
            request.user = data
            next();
        }
    })
}

const verifyAdmin = (req, res, next)=>{
    const user = req.user;
    jwtModel.findOne ({Username: user.username}, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        if(data.Role ==="admin"){
            next()
        }
        else{
            return res.status(500).json({
                status: "you are not premission",
            })
        }
    })
}
module.exports ={
    authenAdminToken,
    verifyAdmin
}
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new  Schema ({
    Name: {
        type: String,
        require: true,
        unique: true
    },
    Type: {
        type: String,
        require: true
    }, 
    ImageUrl: {
        type: String,
        require: true
    },
    BuyPrice: {
        type: Number,
        require: true
    }, 
    PromotionPrice: {
        type: Number,
        require: true
    },
    Description: {
        type: String,
        require: true
    },
    ProductDescription: {
        type: String
    },
    DeliveryFee: {
        type: String
    },

    DeliverTime: {
        type: String
    },
    Warranty: {
        type: String
    },
    Amount: {
        type: Number
    }

});

module.exports = mongoose.model("Products", productSchema);
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const orderSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        ref:"Customers"
    },
    Status:{
        type: String
    },
    Orders:{
        type: []
    }
}, {
    timestamps: true
});
module.exports = mongoose.model("Orders", orderSchema);
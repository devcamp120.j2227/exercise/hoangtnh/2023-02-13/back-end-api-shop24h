const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const customerSchema = new Schema ({
    UserId :{
        type: String,
        unique: true
    },
    Name: {
        type: String,
        required: true
    },
    Email:{
        type: String,
        required: true,
        unique: true
    },
    Birthday:{
        type: String,
    },
    Phone:{
        type: String
    },
    Gender:{
        type: String
    },
    Address:{
        type: {}
    },
    Orders:{
        type: []
    },
    Carts:[{
        type: mongoose.Types.ObjectId,
        ref: "Orders"
    }]
});
module.exports = mongoose.model("Customers", customerSchema);
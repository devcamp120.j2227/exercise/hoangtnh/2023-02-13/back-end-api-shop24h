const express = require("express");
const router = express.Router();

const jwtController = require("../controllers/jwtController");
const jwtMiddleware = require("../middlewares/jwtMiddleware");
router.post("/accounts", jwtController.createNewAccount);
router.put("/admins/:username", jwtMiddleware.authenAdminToken, jwtController.editAdministrator)
router.get("/accounts", jwtController.getAllAccount);
router.post("/login", jwtController.loginAccount);
router.post("/refreshTokens", jwtController.refreshToken);
router.post("/logout", jwtController.logoutAccount);
router.post("/loginWithEncode", jwtController.loginAccountWithEncodePass);
router.put("/forgotPass", jwtController.editPassword);
module.exports = router;
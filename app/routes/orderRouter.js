const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");
const jwtMiddleware =  require("../middlewares/jwtMiddleware");

router.post("/orders/post", orderController.createOrder);
router.get("/orders/:orderId", orderController.getOrderById);
router.get("/orders",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.getAllOrder);
router.get("/ordersCustomer/:orderId", orderController.findCustomerInfoByOrderId);
router.get("/orders/filter/:orderId",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.filterOrder);
router.get("/filter",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.filterOrderByStatus);
router.put("/orders/edit",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.updateStatusOrder);
router.delete("/orders/delete",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.deleteOrder);
router.get("/orders/phoneNumber/:phone",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.findOrderByPhoneNumber);
router.get("/filterDate/dateOrder",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, orderController.findOrderByDate);
module.exports = router;
const express = require("express");
const router = express.Router();

const customerController = require("../controllers/customerController");
const jwtMiddleware =  require("../middlewares/jwtMiddleware");

router.post("/customers", customerController.createCustomer);
router.post("/customers-admin-page",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, customerController.createCustomerAdminPage)
router.get("/customers", customerController.getCustomerByUserId);
router.put("/customers", customerController.updateInforCustomer);
router.get("/allcustomers",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, customerController.getAllCustomer);
router.get("/customer&orders",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, customerController.getCustomerAndOrdersByUserId);
router.get("/email-customer",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, customerController.getCustomerByEmail);
module.exports = router;
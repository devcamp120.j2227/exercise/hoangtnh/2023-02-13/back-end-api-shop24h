const mongoose = require("mongoose");
const orderModel = require("../model/orderModel");
const customerModel = require("../model/customerModel");
//fuction tạo thông tin customer mới
const createOrder = (request, response) =>{
    const body = request.body;
    const userId = request.query.userId;
    
    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        Status: "Ordered",
        Orders: body.Orders
    };
    customerModel.findOne({UserId:userId}, (errorId, idExist) => {
        if (errorId) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorId.message
            })
        }
            orderModel.create(newOrder,(error,data)=>{
                if(error){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: error.message
                    })
                }
                customerModel.findOneAndUpdate({UserId:userId},{
                    $push:{
                        Carts: data._id
                    }
                }, {new:true}, (err, updateCustomer)=>{
                    if(err){
                        return response.status(500).json({
                            status: "Internal server error",
                            message: err.message
                        })
                    }
                        return response.status(201).json({
                            status:"Create order with customer id successfully",
                            data: data
                        })
                    })
                
            })
        
    })
    
};
const getOrderById = (request, response) =>{
    const orderId = request.params.orderId;
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }
    orderModel.findById(orderId,(error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
            return response.status(200).json({
                status: "Get detail order successfully",
                data: data
            })
        })
}
//find order by short id
const filterOrder = (request, response) =>{
    const orderId = request.params.orderId;
    orderModel.aggregate([
        {
          $addFields: {
            tempId: { $toString: '$_id' },
          }
        },
        {
          $match: {
            tempId: { $regex: `${orderId}`}
          }
        }
      ],(error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
            return response.status(200).json({
                status: "Get detail order successfully",
                data: data
            })
        })
}
//find order by status of order
const filterOrderByStatus = (req, res) =>{
    const statusOrder = req.query.statusOrder;
    if(statusOrder){
    orderModel.find({Status: statusOrder} ,(errorFindOrder, OrderFound) => {
        if(errorFindOrder){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindOrder.message
            })
        } else{
            if(!statusOrder){ 
                return res.status(400).json({
                    status:" order is not valid",
                    data: []
                })
            } 
            else {
                orderModel.find({Status: statusOrder} ,(error, data) =>{
                    if(error){
                        return res.status(500).json({
                            status:" Internal server error",
                            message: error.message
                        })
                    }
                        return res.status(200).json({
                            status:`Get order successfully`,
                            data: data
                        })
                })
            }
        }
    })
    }
}
const getAllOrder = (req, res) =>{
    const skipNumber = req.query.skipNumber;
    const limitNumber = req.query.limitNumber;

    if(skipNumber){
        orderModel.find()
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((error, data)  => {
                    if(error) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    } 
                    else {
                        return res.status(200).json({
                            status: "Success: Get skip limit orders success",
                            data: data
                        })
                    }
        })
    }
    else{
        orderModel.find((error, data)=>{
            if(error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
                return res.status(200).json({
                    status: "Get all orders successfully",
                    data: data
                })
        })
    }

}
const findCustomerInfoByOrderId = (req, res) =>{
    const orderId = req.params.orderId
    customerModel.findOne({Carts: orderId},(error, data)=>{
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
            return res.status(200).json({
                status: "Get customer by orderid successfully",
                data: data
            })
    })
}
//thay đổi trạng thái order
const updateStatusOrder = (req, res) =>{
    const orderId = req.query.orderId;
    const status = req.body.status;
    orderModel.findOneAndUpdate({_id : orderId}, {Status:status},{new: true}, (error, data)=>{
        if(error){
            return res.status(500).json({
                status:"Internal server error", 
                message: error.message
            })
        }
        return res.status(201).json({
            status:"Update order status successfully",
            data: data
        })
    })
}
//xóa order 
const deleteOrder = (req, res) =>{
    const orderId = req.query.orderId;
    const userId = req.query.userId
    orderModel.findByIdAndRemove({_id : orderId},{new: true}, (error, data)=>{
        if(error){
            return res.status(500).json({
                status:"Internal server error", 
                message: error.message
            })
        }
        customerModel.findByIdAndUpdate(userId, {
            $pull: { Carts: orderId }
        },{new: true}, (err, updatedCustomer) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(200).json({
                status: "Delete order successfully"
            })
        })
    })
}
const findOrderByPhoneNumber = (req, res)=>{
    const phone = req.params.phone;
    
    customerModel.findOne({Phone:  { $regex: phone}},(error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        if(data === null){
            return res.status(400).json({
                status: "Bad request",
                message: "Phone number is not valid"
            })
        }
            return res.status(200).json({
                status: "Get order by phone number successfully",
                data: data
            })
        })
}
const findOrderByDate = (req, res) =>{
    const startDate = req.query.startDate;
    const endDate = req.query.endDate;
    const skipNumber = req.query.skipNumber;
    const limitNumber = req.query.limitNumber;  
    //nếu trường hợp start date < end date (true)
    if(new Date(startDate).getTime() < new Date(endDate).getTime()){
        console.log("true")
        orderModel.find({
            createdAt:{
                $gte: startDate, 
                $lt: new Date(endDate).setDate(new Date(endDate).getDate() + 1)
            }
        })
        .skip(skipNumber)
        .limit(limitNumber)
        .exec((error, data)  => {
                if(error) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: error.message
                    })
                } 
                else {
                    return res.status(200).json({
                        status: "Success: Get filter by date & limit orders success",
                        data: data
                    })
                }
            })
    }
    //nếu chỉ nhập mỗi start date
    if(new Date(startDate).getTime() === new Date(endDate).getTime()){
        console.log("miss end date")
        orderModel.find({
            createdAt:{
                $gte: startDate, 
                $lt: new Date(startDate).setDate(new Date(startDate).getDate() + 1)
            }
        })
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((errorFound, data)  => {
                if(errorFound) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: errorFound.message
                    })
                } 
                else {
                    return res.status(200).json({
                        status: "Success: Get filter by date & limit orders success",
                        data: data
                    })
                }
            })
    }
    //nếu start date > end date
    if(new Date(startDate).getTime() > new Date(endDate).getTime()){
        console.log("false")
    orderModel.find({
        createdAt:{
            $gte: endDate, 
            $lt: new Date(startDate).setDate(new Date(startDate).getDate() + 1)
        }
    })
    .skip(skipNumber)
    .limit(limitNumber)
    .exec((error, data)  => {
            if(error) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } 
            else {
                return res.status(200).json({
                    status: "Success: Get filter by date & limit orders success",
                    data: data
                })
            }
        })
    }
}
module.exports ={
    createOrder,
    getOrderById,
    getAllOrder,
    findCustomerInfoByOrderId,
    filterOrder,
    filterOrderByStatus,
    updateStatusOrder,
    deleteOrder,
    findOrderByPhoneNumber,
    findOrderByDate
}
const { default: mongoose } = require("mongoose");
const productModel = require("../model/productModel");
//function tạo product mới
const createProduct = (request, response) =>{
    const body = request.body;

    if(!body.Name){
        return response.status(400).json({
            status:"Bad Request",
            message: "Name Product không hợp lệ"
        })
    }
    if(!body.Type){
        return response.status(400).json({
            status:"Bad Request",
            message: "Type Product không hợp lệ"
        })
    }
    if(!body.ImageUrl){
        return response.status(400).json({
            status:"Bad Request",
            message: "ImageUrl Product không hợp lệ"
        })
    }
    if(!body.BuyPrice){
        return response.status(400).json({
            status:"Bad Request",
            message: "BuyPrice Product không hợp lệ"
        })
    }
    if(!body.PromotionPrice){
        return response.status(400).json({
            status:"Bad Request",
            message: "PromotionPrice Product không hợp lệ"
        })
    }

    const newProduct = {
        Id: body.Id,
        Name: body.Name,
        Type: body.Type,
        ImageUrl: body.ImageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description,
        ProductDescription: body.ProductDescription,
        DeliveryFee: body.DeliveryFee,
        DeliverTime:body.DeliverTime,
        Warranty: body.Warranty,
        Amount: body.Amount,
    }
    productModel.create(newProduct,(error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            message:"Create new product successfully",
            Product: data
        })
    })
}
//tạo function get product
const getAllProduct = (request, response) => {
    productModel.find((error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all Products successfully",
            data: data
        })
    })
}
const getProductByDescription = (request, response)=> {
    const DescriptionProduct =  request.query.DescriptionProduct;
    if(DescriptionProduct){
    productModel.find({Description: DescriptionProduct}, (errorFindProduct, DescriptionProductExist) => {
        if(errorFindProduct){
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindProduct.message
            })
        } else{
            if(!DescriptionProduct){ 
                return response.status(400).json({
                    status:" Type product is not valid",
                    data: []
                })
            } else {
                productModel.find({Description: DescriptionProduct} ,(error, data) =>{
                    if(error){
                        return response.status(500).json({
                            status:" Internal server error",
                            message: error.message
                        })
                    }
                    return response.status(200).json({
                        status:`Get product by ${DescriptionProduct} successfully`,
                        Product: data
                    })
                })
            }
        }
    })
    }
}
const getProductByCondition = (request, response) =>{
    const type = request.query.type;
    const description = request.query.description;
    const minPrice = request.query.minPrice;
    const maxPrice = request.query.maxPrice;
    const skipNumber = request.query.skipNumber;
    const limitNumber = request.query.limitNumber;
    const nameProduct = request.query.nameProduct
    //tạo điều kiện để thu thập dữ liệu filter
    const condition = {};
    //filter bằng description
    if(description){
           condition.Description = description
    }
    //filter bằng type
    if(type){
            condition.Type = type
    }
    if(nameProduct){
        condition.Name = nameProduct
    }
    //filter bằng price min 
    if(minPrice){
        condition.PromotionPrice = {
        ...condition.PromotionPrice,
        $gte: minPrice
        }
    }
    //filter bằng price max
    if(maxPrice){
        condition.PromotionPrice = {
        ...condition.PromotionPrice,
        $lte: maxPrice
        }
    }
    console.log(condition) //check các điều kiện đã nhận được
    productModel.find(condition)
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((error,data)=>{
                if(error){
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: error.message
                    })
                }
                else{
                    return response.status(200).json({
                        status:"Get filter product successfully",
                        data: data
                    })
                }
            })
 
}
//530.30 call api limit product shop 24h
const getProductLimit = (request, response) =>{
    const limitNumber = request.query.limitNumber;
    if(limitNumber){
        productModel.find()
            .limit(limitNumber)
            .exec((err,data) =>{
                if(err){
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: err.message
                    })
                }
                else{
                    return response.status(200).json({
                        status:"Get limited product successfully",
                        data: data
                    })
                }
            })}
        else{
            productModel.find((error, data) =>{
                if(error){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: error.message
                    })
                }
                return response.status(200).json({
                    status:"Get all Products successfully",
                    data: data
                })
            })
        }
    
}
const getProductsSkipLimit = (request, response) =>{
    const skipNumber = request.query.skipNumber;
    const limitNumber = request.query.limitNumber;

    if(skipNumber){
        productModel.find()
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((error, data)  => {
                    if(error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    } 
                    else {
                        return response.status(200).json({
                            status: "Success: Get skip limit products success",
                            data: data
                        })
                    }
                })
                
    }
    else{
            productModel.find((error, data) =>{
                if(error){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: error.message
                    })
                }
                return response.status(200).json({
                    status:"Get all Products successfully",
                    data: data
                })
            })
        }
}
//get product by id
const getProductById = (request,response) =>{
    const productId = request.params.productId
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return response.status(400).json({
            status:"Bad request",
            message:"product id is not valid"
        })
    }
    productModel.findById(productId, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get product successfully`,
            data: data
        })
    })
}
//edit product detail by product id
const editProduct = (req, res) =>{
    const productId = req.params.productId;
    const body = req.body;
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return response.status(400).json({
            status:"Bad request",
            message:"product id is not valid"
        })
    }
    const updateProduct = {
        Name: body.Name,
        Type: body.Type,
        ImageUrl: body.ImageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description,
        ProductDescription: body.ProductDescription,
        DeliveryFee: body.DeliveryFee,
        DeliverTime:body.DeliverTime,
        Warranty: body.Warranty,
        Amount: body.Amount
    }
    productModel.findByIdAndUpdate(productId, updateProduct, {new:true}, (error, data)=>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            status:"Update Product successfully",
            data: data
        })
    })
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductByDescription,
    getProductLimit,
    getProductsSkipLimit,
    getProductByCondition,
    getProductById,
    editProduct
};

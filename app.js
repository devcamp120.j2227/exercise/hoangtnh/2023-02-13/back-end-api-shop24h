const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors')
const app = express();
app.use(express.json());
app.use(cors());
require("dotenv").config();
var port = process.env.PORT || 8080;

mongoose.connect("mongodb+srv://goldenfrog:dinosaur193@hoang.1ok6g1k.mongodb.net/project_shop_24h",(error)=>{
    if(error) throw error;
    console.log("Connected to mongo Atlas")
}) 
// mongoose.connect("mongodb://127.0.0.1:27017/project_shop24h",(error)=>{
//     if(error) throw error;
//     console.log("Connect to database Shop24h successfully")
// }) 
app.get('/', (req, res, next) => {

    res.status(200).json({
        status: 'success',
        data: {
            name: 'name of your app',
            version: '0.1.0'
        }
    });

});
const productRouter = require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter");
const orderRouter = require("./app/routes/orderRouter");
const jwtRouter = require ("./app/routes/jwtRouter");
app.use("/api", productRouter);
app.use("/api", customerRouter);
app.use("/api", orderRouter);
app.use("/api", jwtRouter);

app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})